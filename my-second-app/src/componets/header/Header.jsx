import React, { Component } from 'react';
import './Header.scss';

class Header extends Component{
    render(){
        return(
            <div className='header'>
                <span className='header__text'>Buy</span>
                <img className='header__logo' src='img/toy-logo.png' alt='black toy' />
                <span className='header__text'>Toys</span>
            </div>

        );
    }
}

export default Header;