import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Product from "../product/Product";
import './ProductList.scss'

class ProductList extends Component {
    static propTypes = {
        products: PropTypes.array,
        displayModal: PropTypes.func,
        addToFavorite: PropTypes.func,
        favorites: PropTypes.array,
    }

    render() {
        const { products, displayModal, addToFavorite, favorites } = this.props

        return (
            <>
                <div className='products'>
                    <div className='products__wrapper'>
                        {products.map(product => {
                            return <Product
                                favorites={favorites}
                                key={product.SKU}
                                productInfo={product}
                                displayModal={displayModal}
                                addToFavorite={addToFavorite}
                            />
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default ProductList;