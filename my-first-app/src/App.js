import React, { Component } from 'react';
import Button from './elements/button/Button';
import Modal from './elements/modal/Modal';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      modalContent: {},
    };
  }

  showModal = () => {
    this.setState({ modalShow: true });
  };

  generateModal = (modalInfoObject) => {
    this.showModal();
    this.setState({ modalContent: modalInfoObject });
  };

  closeModal = () => {
    this.setState({ modalShow: false, modalContent: {} });
  };

  render() {
    return (
        <>
          <Button modalID="modalID1" title="Open first modal" onClick={this.generateModal} backGround="red" />
          <Button modalID="modalID2" title="Open second modal" onClick={this.generateModal} backGround="blue" />
          {this.state.modalShow && <Modal modalContent={this.state.modalContent} onClose={this.closeModal} />}
        </>
    );
  }
}

