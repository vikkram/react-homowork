const types = {
	getProductsRequested: 'GET_PRODUCTS_REQUESTED',
	getProductsSuccess: 'GET_PRODUCTS_SUCCESS',
	getProductsError: 'GET_PRODUCTS_ERROR',
	setFavorites: 'SET_FAVORITES',
	setCart: 'SET_CART',
	setModal: 'SET_MODAL',
	confirmCartPurchase: 'CONFIRM_CART_PURCHASE',
}

export default types