import types from "../types";

const initialState = {
	noModal: true,
	productData: {},
	modalType: null,
}

const modalInfoReducer = (state = initialState, action) => {
	switch (action.type) {
		case types.setModal: {
			return action.payload
		}
		default: {
			return state
		}
	}
}

export default modalInfoReducer