import types from "../types";

const setCart = (cartItems) => {
	return {
		type: types.setCart,
		payload: {
			cart: cartItems
		}
	}
}

const confirmCartPurchase = () => {
	localStorage.setItem('cart', JSON.stringify([]))
	return {
		type: types.confirmCartPurchase,
		payload: null
	}
}

export { setCart, confirmCartPurchase}