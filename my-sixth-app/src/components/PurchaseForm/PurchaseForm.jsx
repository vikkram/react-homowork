import React from 'react';
import './purchaseForm.scss'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { formSchema } from '../../schemas/index';
import { confirmCartPurchase } from '../../store/cart/action';


const PurchaseForm = () => {
	const cart = useSelector(state => state.cart)
	const dispatch = useDispatch()
	const totalOrderPrice = cart.reduce((prev, cartItem) => prev + cartItem.price * cartItem.quantity, 0)

	const handleSubmit = (data, bag) => {
		console.log(data)
		console.log(cart)
		dispatch(confirmCartPurchase())
	}

	return (
		<div>
			<h3 className='purchase-title'>Place your order for: {totalOrderPrice} USD</h3>
			<Formik
				initialValues={{
					name: '',
					surname: '',
					age: '',
					address: '',
					phone: '',
				}}
				onSubmit={handleSubmit}
				validationSchema={formSchema}
			>{({ errors, touched }) => {
				return (
					<Form>
						<div className='input-wrapper'>
							<label htmlFor='name' className='input-text'>Enter your name</label>
							<Field
								className={errors.name && touched.name ? 'input input-error' : 'input'}
								id='name'
								name='name'
								type='text'>
							</Field>
							<ErrorMessage name='name' component={'div'} className='input-error-message'></ErrorMessage>
						</div>

						<div className='input-wrapper'>
							<label htmlFor='surname' className='input-text'>Enter your surname</label>
							<Field
								className={errors.surname && touched.surname ? 'input input-error' : 'input'}
								id='surname'
								name='surname'
								type='text'>
							</Field>
							<ErrorMessage name='surname' component={'div'} className='input-error-message'></ErrorMessage>
						</div>

						<div className='input-wrapper'>
							<label htmlFor='age' className='input-text'>Enter your age</label>
							<Field
								className={errors.age && touched.age ? 'input input-error' : 'input'}
								id='age'
								name='age'
								type='number'>
							</Field>
							<ErrorMessage name='age' component={'div'} className='input-error-message'></ErrorMessage>
						</div>

						<div className='input-wrapper'>
							<label htmlFor='address' className='input-text'>Enter your address</label>
							<Field
								className={errors.address && touched.address ? 'input input-error' : 'input'}
								id='address'
								name='address'
								type='text'>
							</Field>
							<ErrorMessage name='address' component={'div'} className='input-error-message'></ErrorMessage>
						</div>

						<div className='input-wrapper'>
							<label htmlFor='phone' className='input-text'>Enter your phone</label>
							<Field
								className={errors.phone && touched.phone ? 'input input-error' : 'input'}
								id='phone'
								name='phone'
								type='tel'>
							</Field>
							<ErrorMessage name='phone' component={'div'} className='input-error-message'></ErrorMessage>
						</div>

						<button className='submit-form' type='submit'>Checkout</button>
					</Form>)
			}
				}
			</Formik>
		</div>
	);
};

export default PurchaseForm;