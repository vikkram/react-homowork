import './header.scss';

const Header = () => {
	return (
		<div className='header'>
			<div className='header__text'>Buy</div>
			<img className='header__logo' src='img/toy-logo.png' alt='black toy' />
			<div className='header__text'>Toys</div>
		</div>
	);
}

export default Header;