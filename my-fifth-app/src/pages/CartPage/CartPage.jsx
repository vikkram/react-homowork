import React from 'react';
import { useSelector } from 'react-redux';
import Product from '../../components/Product/Product';
import PurchaseForm from '../../components/PurchaseForm/PurchaseForm';
import './cartPage.scss';

const CartPage = () => {
	const cart = useSelector(state => state.cart)

	return (
		<>
			{
				cart.length === 0 &&
				<div className='cart-empty'>No items have been added to cart.</div>
			}
			<div className='cart'>
				{
					cart.length !== 0 &&
					<div className='cart__form'>
						<PurchaseForm />
					</div>
				}
				<div className='cart__products'>
					{cart.length > 0 &&
						cart.map((cartItem) => {
							return <Product
								key={`${cartItem.SKU}C`}
								product={cartItem}
								inCart={true} />
						})
					}
				</div>
			</div>
		</>
	);
};

export default CartPage;