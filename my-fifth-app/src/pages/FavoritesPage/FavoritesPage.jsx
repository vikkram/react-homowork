import React from 'react';
import './favoritesPage.scss';
import Product from '../../components/Product/Product';
import { useSelector } from 'react-redux';

const FavoritesPage = () => {
	const favorites = useSelector(state => state.favorites)

	return (
		<>
			{favorites.length === 0 &&
				<div className='favorites-empty'>No items have been added to favorites.</div>}
			<div className='favorites'>
				<div className='favorites__wrapper'>
					{favorites.length > 0 &&
						favorites.map(favorite => {
							return <Product
								key={`${favorite.SKU}F`}
								product={favorite}
								isFavorite={true} />
						})
					}
				</div>
			</div>
		</>
	);
};

export default FavoritesPage;