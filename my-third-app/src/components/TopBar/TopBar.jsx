import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './topBar.scss';

const TopBar = ({ favorites = [], cart = [] }) => {
	return (
		<div className='top-bar'>
			<div className='home'>
				<Link to={''}>
					<button className='home__button'>Home</button>
				</Link>
			</div>
			<Link to={'favorites'}>
				<div className='favorites'>
					<img src='img/star-added.png' alt='star' />
					<span className='favorites__counter'>{favorites.length}</span>
				</div>
			</Link>
			<Link to={'cart'}>
				<div className='cart'>
					<img src='img/shopping_cart.svg' alt='star' />
					<span className='cart__counter'>{cart.length}</span>
				</div>
			</Link>
		</div>
	);
}

TopBar.propTypes = {
	favorites: PropTypes.array,
	cart: PropTypes.array,
}

export default TopBar;