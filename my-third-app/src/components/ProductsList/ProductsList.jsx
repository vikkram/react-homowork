import PropTypes from 'prop-types';
import Product from '../Product/Product';
import './productsList.scss';

const ProductsList = (props) => {
	const { products, displayModal, toggleFavorites, favorites } = props

	const handleAddToFavoritesClick = (SKU) => {
		toggleFavorites(SKU)
	}

	const handleDisplayModalClick = (SKU, type) => {
		displayModal(SKU, type)
	}

	return (
		<>
			<div className='products'>
				<div className='products__wrapper'>
					{products.map(product => {
						return <Product
							favorites={favorites}
							key={product.SKU}
							productInfo={product}
							onDisplayModalClick={handleDisplayModalClick}
							onFavoriteClick={handleAddToFavoritesClick}
						/>
					})}
				</div>
			</div>
		</>
	);
}

ProductsList.propTypes = {
	products: PropTypes.array,
	displayModal: PropTypes.func,
	addToFavorite: PropTypes.func,
	favorites: PropTypes.array,
}

export default ProductsList;