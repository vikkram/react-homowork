import React, { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import './app.scss';

import getProducts from './api/getProducts';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import TopBar from './components/TopBar/TopBar';
import Footer from './components/Footer/Footer';
import HomePage from './pages/HomePage/HomePage';
import FavoritesPage from './pages/FavoritesPage/FavoritesPage';
import CartPage from './pages/CartPage/CartPage';


const App = () => {
	const [noModal, setNoModal] = useState(true)
	const [modalSettings, setModalSettings] = useState({})
	const [modalType, setModalType] = useState(null)
	const [cartItemIndex, setItemIndex] = useState(null)
	const [products, setProducts] = useState([])
	const [favorites, setFavorites] = useState([])
	const [cart, setCart] = useState([])


	useEffect(() => {
		getProducts()
			.then(allProducts => {
				setProducts(allProducts)
			})

		if (!localStorage.getItem('cart')) {
			localStorage.setItem('cart', JSON.stringify(cart))
		} else {
			setCart(JSON.parse(localStorage.getItem('cart')))
		}

		if (!localStorage.getItem('favorites')) {
			localStorage.setItem('favorites', JSON.stringify(favorites))
		} else {
			setFavorites(JSON.parse(localStorage.getItem('favorites')))
		}
	}, [])

	const displayModal = (cartItem, type, itemIndex) => {
		setModalType(type)
		setNoModal(!noModal)
		setModalSettings(cartItem)

		if (itemIndex >= 0) {
			setItemIndex(itemIndex)
		}
	}

	const closeModal = (event) => {
		event.stopPropagation()
		const conditionToClose = event.target.classList.contains('popup__close')
			|| event.target.classList.contains('popup__body')
			|| event.target.classList.contains('popup__action')

		if (conditionToClose) {
			setNoModal(!noModal)
		}
	}

	const addToCart = (cartItem) => {
		const newCart = [...cart, cartItem]
		setCart(newCart)
		localStorage.setItem('cart', JSON.stringify(newCart))
	}

	const removeFromCart = () => {
		const newCart = cart.filter((el, index) => index !== cartItemIndex)
		setCart(newCart)
		localStorage.setItem('cart', JSON.stringify(newCart))
	}

	const toggleFavorites = (SKU) => {
		const favoritesSKU = SKU
		const allSKU = [...favorites, favoritesSKU]
		const filteredSKU = favorites.filter(el => el !== favoritesSKU)

		if (favorites.includes(favoritesSKU)) {
			setFavorites(filteredSKU)
			localStorage.setItem('favorites', JSON.stringify(filteredSKU))
		} else {
			setFavorites(allSKU)
			localStorage.setItem('favorites', JSON.stringify(allSKU))
		}
	}

	return (
		<div className='content-wrapper'>
			<Header />
			<TopBar favorites={favorites} cart={cart} />
			<div className='main'>
				<Routes>
					<Route path='' element={
						<HomePage
							products={products}
							displayModal={displayModal}
							favorites={favorites}
							toggleFavorites={toggleFavorites}
						/>
					}>
					</Route>
					<Route path='favorites' element={
						<FavoritesPage
							favorites={favorites}
							products={products}
							toggleFavorites={toggleFavorites} />
					}>
					</Route>
					<Route path='cart' element={
						<CartPage
							cart={cart}
							products={products}
							displayModal={displayModal} />
					}></Route>
				</Routes>
			</div>
			<Footer />
			{noModal ? null : <Modal
				modalSettings={modalSettings}
				modalType={modalType}
				closeModal={closeModal}
				addToCart={addToCart}
				removeFromCart={removeFromCart}
			/>}
		</div>
	);
}

export default App;